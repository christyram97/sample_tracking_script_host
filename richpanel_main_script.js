
window['appClientId'] = "hubspottest76825";
window.richpanel_api_url = 'https://api-dev.richpanel.com/v2';
window.richpanel_messenger_url = 'http://messenger-dev-v2.richpanel.com.s3-website-us-west-2.amazonaws.com';
window.richpanelVersion = '2.0.0';

window.richpanel||(window.richpanel=[]),
window.richpanel.queue=[],
window.richpanel.methods=["track","debug","atr"],
window.richpanel.skelet=function(e){
    return function(){
        a=Array.prototype.slice.call(arguments)
        a.unshift(e);
        window.richpanel.queue.push(a)
    }
};
for(var i=0;window.richpanel.methods.length>i;i++){
    var mthd=window.richpanel.methods[i];
    window.richpanel[mthd]=window.richpanel.skelet(mthd)
}
window.richpanel.load=function(e){
    var t=document,
    n=t.getElementsByTagName("script")[0],
    r=t.createElement("script");
    r.type="text/javascript";
    r.async=true;
    var url = "api.richpanel.com/v1";
    // r.src="https://"+url+"/j/"+e+"?version=2.0.0";
    r.src="./internalScript.js"
    n.parentNode.insertBefore(r,n)
};
richpanel.load("richpanel7240")